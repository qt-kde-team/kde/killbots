Source: killbots
Section: games
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Daniel Schepler <schepler@debian.org>,
           Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 6.0.0~),
               gettext,
               libkdegames6-dev,
               libkf6completion-dev (>= 6.0.0~),
               libkf6config-dev (>= 6.0.0~),
               libkf6configwidgets-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6crash-dev (>= 6.0.0~),
               libkf6dbusaddons-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6i18n-dev (>= 6.0.0~),
               libkf6widgetsaddons-dev (>= 6.0.0~),
               libkf6xmlgui-dev (>= 6.0.0~),
               pkg-kde-tools,
               qt6-base-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/killbots
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/killbots.git

Package: killbots
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends: khelpcenter,
Breaks: ${kde-l10n:all},
Replaces: ${kde-l10n:all},
Description: port of the classic BSD console game robots
 killbots is a simple game of evading killer robots. The robots are numerous
 and their sole objective is to destroy you.  Fortunately for you, their
 creator has focused on quantity rather than quality and as a result
 the robots are severely lacking in intelligence. Your superior wit and
 a fancy teleportation device are your only weapons against the never-ending
 stream of mindless automatons.
 .
 This package is part of the KDE games module.
